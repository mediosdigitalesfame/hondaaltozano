<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

	<title>Contacto</title>

	<?php include('contenido/head.php'); ?>

</head>



<body>

	<?php include('chat.php'); ?>


	<div id="container">

		 <?php include('contenido/header.php'); ?>

		 <?php include('contenido/analytics.php'); ?>



		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">

				<div class="container">

					<h2>Contáctanos</h2>



				</div>

			</div>





			<!-- contact box -->

			<div class="contact-box">

				<div class="container">

					<div class="row">

                        

                        <div class="col-md-6" align="center">

                         <div class="container">

					         <div class="col-md-12" >

								 <?php include('jotform.php'); ?>

                             </div>

                         </div>

                     </div>

                        

						<div class="col-md-3">

							<div class="contact-information">

								<h3>Información de Contacto</h3>

								<ul class="contact-information-list">

									<li><span><i class="fa fa-home"></i>Paseo Altozano; Zona Automotriz.</span> <span>Av. Montaña Monarca #1000 </span> <span>Col. Desarrollo Montaña Monarca</span><span> Morelia, Michoacán</span></li>

									<li><span><i class="fa fa-phone"></i>(443) 340 4537</span></li>

                                    <li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>(443) 340 4538</strong></span><br>

                                   	<i class="fa fa-phone"></i><span>Refacciones <strong>(443) 340 4537</strong></span><br>

                                    <i class="fa fa-phone"></i><span>Postventa <strong>(443) 340 4537</strong></span><br>

                                    <i class="fa fa-phone"></i><span>Ventas <strong>(443) 340 4538</strong></span><br>                

                                    <i class="fa fa-phone"></i><span>Recepción <strong>(443) 340 4537</strong></span><br></li>

                                    

                                    <h3>Whatsapp</h3>

                       				<li><span><i class="fa fa-whatsapp"></i><strong>Ventas y  Postventa <br> 4432732461 |   5554186443 </strong></span></li>

                                    

<!--									<li><a href="#"><i class="fa fa-envelope"></i>contacto@hondacamelinas.com</a></li>-->

								</ul>

							</div>

						</div>



						<div class="col-md-3">

							<div class="contact-information">

								<h3>Horario de Atención</h3>

								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda Altozano</strong>; te escuchamos y atendemos de manera personalizada. </p>

								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>

								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>

							</div>

						</div>



 </div>

			 </div>



			 <?php include('contenido/vehiculos.php'); ?>

			 

		 </div>



		</div> 



		<br>



			<?php include('contenido/footer.php'); ?>

     </div> 			

	

</body>

</html>