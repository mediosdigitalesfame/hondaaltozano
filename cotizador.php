<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cotizador de Auto</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="FAME HONDA Altozano Morelia. Sitio WEB oficial FAME Altozano Morelia. Conoce al grupo automotriz más grande de México y Latinoamérica, con más de 1600 unidades en inventario. Piensa en auto, piensa en FAME. ">
    <meta name="keywords" content="Honda Altozano, honda fame, fame, Grupo Fame, Honda Morelia, Morelia, frame altozano, fame Altozano, honda Michoacan, honda Mexico, Autos, Nuevos, Seminuevos, Agencia, Servicio, Taller, Hojalatería, hojalateria, Pintura, postventa, accord, accord coupe, accord sedan, city, honda city, civic, civic coupe, civic sedan, civic si, civic hybrid, crosstour, cr-v, crv, cr-z, crz, fit, honda fit, odyssey, pilot, honda pilot, ridgeline, 2015, Lujo, honda, The power of Dreams, ASIMO, Premium, 2014">
    <meta name="author" content="Alejandro Cruz Saucedo">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    

    <link rel="icon" type="image/png" href="/images/favicon.png" />

</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
						<span><i class="fa fa-phone"></i>Agencia: (443) 340 4537 </span>
						</p>
						<ul class="social-icons">
							<li><a class="facebook" href="https://www.facebook.com/Honda-Altozano-1692902814346774" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/HondaFame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                             <li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
                            <li><a class="whatsapp" href="http://hondaaltozano.com/contacto.php" target="_self"><i class="fa fa-whatsapp"></i></a></li>
						</ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="index.html">Inicio</a></li>
							<!--<li class="drop"><a>Financiamiento</a>
								 </li>  -->
							<li class="drop"><a class="active" href="#">Servicio</a>
								<ul class="drop-down">
									<li><a href="servicio.html">Servicio</a></li>
									<li><a href="servicio.php">Cita de Servicio</a></li>                     
									<li><a href="garantia.html">Garantía Extendida</a></li>
									<li><a href="refacciones.php">Refacciones</a></li>
									<li><a href="privilegios.html">Club de Privilegios</a></li>
                                 </ul> </li>   
                                                                                            
							<li class="drop"><a href="autos.html">Vehículos</a>
								<ul class="drop-down">
                                
                                	<li><a href="fichas/fit.pdf" target="_blank">FIT</a></li>
                                	<li><a href="fichas/city.pdf" target="_blank">CITY</a></li>
                                	<li><a href="fichas/civic_sedan.pdf" target="_blank">CIVIC SEDÁN</a></li> 
                                    <li><a href="civic-coupe.php" target="_self">CIVIC COUPÉ</a></li>
                                	<li><a href="fichas/accord_sedan.pdf" target="_blank">ACCORD </a></li>
                                	<li><a href="fichas/crv.pdf" target="_blank">CR-V</a></li>
                                    <li><a href="fichas/hrv.pdf" target="_blank">HR-V</a></li>
                                    <li><a href="fichas/pilot.pdf" target="_blank">PILOT</a></li>
                                    <li><a href="fichas/odissey.pdf" target="_blank">ODYSSEY</a></li>
                                </ul> </li>
                                                      
							<li class="drop"><a href="promociones.html">Promociones</a>  
                            		<ul class="drop-down">
									<li><a href="promociones.html">Promociones Vigentes</a></li>
                                    <li><a href="finance.html">Honda Finance</a></li>
									<li><a href="planes.html">Planes - Comisiones</a></li>
									<li><a href="autofinanciamiento.php" target="_blank">Autofinanciamiento</a></li>																																				
                                  </ul>
                            
                              </li>                        
							<li><a href="http://www.fameseminuevos.com/index.php/agencia/agencia/1" target="_blank">Seminuevos</a></li>
							<li><a href="contacto.php">Contacto</a></li>
                            <li><a href="ubicacion.html">Ubicación</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->
        
<!-- ANALYTICS -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47752252-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- FIN ANALYTICS -->  

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Cotiza tu Auto</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
                
                

						<div class="col-md-6" align="center">
							<h3>Cotiza tu próximo  HONDA</h3>
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "asesordigital@hondamorelia.com" ;
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Cotizador Auto FAME Honda Altozano";
			$asunto = "Cotizador Auto Honda Altozano";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <br><br><br>

<?php if(isset($result)) { echo $result; } ?>
    </form><br><br><br>

						</div>
                
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">



<!--
				<div class="welcome-box">
					<div class="container">
                
						<h1><span>Partes Originales</span></h1><br>
						<p align="justify">Atienda su HONDA como se merece, le ofrecemos refacciones originales a los mejores precios del mercado. Contamos con un extenso surtido de refacciones originales que nos permiten ofrecerle siempre lo que usted necesita.</p><br>
                 
<p align="left">Proveemos y comercializamos las refacciones y accesorios originales ya sea por mostrador o a través del Taller de Servicio.</p><br><br>

   
<p align="justify"><strong>Los beneficios de adquirir sus refacciones con nosotros son:</strong></p><br>

<p align="justify">- Partes 100% originales con garantía<br>
- Amplio inventario de partes<br>
- Accesorios originales autorizados por HONDA para no perder tu garantía<br>
- Mayor resistencia al desgaste<br>
- Precios altamente competitivos para nuestros clientes<br>
- Excelente Atención<br>
- Promociones durante todo el año<br>
- Servicio de envío por paquetería<br>
</p><br><br>

<p align="justify">Vale la pena invertir en refacciones originales, ya que representan significativos ahorros a largo plazo y mejor funcionamiento de su vehículo.</p><br><br>


<p align="left"><strong>Horarios de atención</strong></p><br>   
<p align="justify">
- Lunes a Viernes.  8:30 a 19:00 hrs<br>
- Sábados.  9:00 a 14:00 hrs<br><br></p><br>


                	</div>
				</div>
                -->
 
               </div>
             </div>
 


		<!-- footer 
			================================================== -->
		<footer>
			<div class="footer-line">
				<div class="container">
					<p><span><i class="fa fa-phone"></i> 01800 670 8386 |</span> 2016 FAME Honda Altozano Morelia | <i class="fa fa-user"> </i><a href="aviso.html" target="_self"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>
		<!-- End footer -->
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</body>
</html>